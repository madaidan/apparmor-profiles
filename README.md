# Apparmor Profiles

These are apparmor profiles that can greatly increase security and protect against exploits and malware.

I've only tested these on Arch Linux but they should work on other Linux distros.

## How to use

Place whichever apparmor profile you want in /etc/apparmor.d/ and run `aa-enforce (profile)` as root.